import { useEffect } from 'react';

const UseClickOutside = (ref: React.MutableRefObject<any>, callback: () => void) => {
    const handleClick = (e: MouseEvent) => {
        if (ref.current && !ref.current.contains(e.target)) {
            callback();
        }
    };
    // создаем слушатель события
    useEffect(() => {
        document.addEventListener('mousedown', handleClick);
        //удаляет слушателя события mousedown, чтобы избежать утечек памяти и нежелательного выполнения событий
        return () => {
            document.removeEventListener('mousedown', handleClick);
        };
    }, [ref, callback]);
};

export default UseClickOutside;
