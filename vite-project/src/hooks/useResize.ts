import { useState, useEffect } from 'react';

// Определение типа возвращаемого значения хука
interface UseWindowWidth {
  width: number;
}

function useResize(): UseWindowWidth {
  const [width, setWidth] = useState<number>(window.innerWidth);

  useEffect(() => {
    // Функция, которая обновит состояние при изменении размера окна
    const handleResize = () => {
      setWidth(window.innerWidth);
    };

    // Подписываемся на событие resize
    window.addEventListener('resize', handleResize);

    // Отписываемся от события при размонтировании компонента
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []); // Пустой массив зависимостей, чтобы эффект выполнился один раз при монтаже

  return { width };
}

export default useResize;