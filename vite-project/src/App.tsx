import React, { createContext, useContext, useMemo, useState, useEffect } from 'react'
import './App.css'
// import Fetch from './components/Fetch/Fetch';
import DevelopersList from './components/List/DevelopersList';
import { getAllDevelopers }  from './services/developerService';
import { Developer } from "./models/Developer"
import Modal from './components/Modal/Modal';
import MyContext from './components/MyContext/MyContext';
import MyRef from './components/MyRef/MyRef';
import MyId from './components/MyId/MyId';
import MyCallback from './components/MyCallback/MyCallback';
import MyMemo from './components/MyMemo/MyMemo';
import MyEffect from './components/MyEffect/MyEffect';
import MyMultyHooksComponent from './components/MyMultyHooksComponent/MyMultyHooksComponent';
import MyResize from './components/MyResize/MyResize';

const MyContextApp = createContext('Default Value app');

const MyComponent = () => {
  const value = useContext(MyContextApp);

  return <div>{value}</div>;
};

// context optimization
interface User {
  name: string;
  age: number;
}

interface Theme {
  color: string;
  backgroundColor: string;
}

const UserContext = createContext<User | undefined>(undefined);
const ThemeContext = createContext<Theme | undefined>(undefined);

interface ProviderProps {
  children: React.ReactNode;
}

const UserProvider: React.FC<ProviderProps> = ({ children }) => {
  const user = useMemo(() => ({ name: 'John', age: 30 }), []);
  return <UserContext.Provider value={user}>{children}</UserContext.Provider>;
};

const ThemeProvider: React.FC<ProviderProps> = ({ children }) => {
  const theme = useMemo(() => ({ color: 'blue', backgroundColor: 'white' }), []);
  return <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>;
};

const UserInfo: React.FC = React.memo(() => {
  const user = useContext(UserContext);
  if (!user) {
    throw new Error('UserInfo must be used within a UserProvider');
  }
  return <div>Name: {user.name}</div>;
});

const ThemeButton: React.FC = React.memo(() => {
  const theme = useContext(ThemeContext);
  if (!theme) {
    throw new Error('ThemeButton must be used within a ThemeProvider');
  }
  return <button style={{ color: theme.color, backgroundColor: theme.backgroundColor }}>Button</button>;
});

// context optimization


function App() {
  const developers = getAllDevelopers();
  const [visibleData, setVisibleData] = useState<Developer[]>([]);

  useEffect(() => {
    setVisibleData(developers)
  }, [developers]);


  const updateDevelopers = (updatedDevelopers : Developer[]) => {
    setVisibleData([...updatedDevelopers]);
  }

  return (
    <>
      <div className="card">
      </div>
      <div>
          <DevelopersList
            developers={visibleData}
            onDelete={updateDevelopers}
          >
          </DevelopersList>
        </div>
        <p>контекст</p>
        <MyContext />
        <MyComponent />
        {/* context optimization */}
        <p>оптимизация контекста</p>
        <UserProvider>
          <ThemeProvider>
            <UserInfo />
            <ThemeButton />
          </ThemeProvider>
        </UserProvider>
        <p>ссылка</p>
        <MyRef />
        <p>идентификатор</p>
        <MyId />
        <p>коллбек</p>
        <MyCallback />
        <p>мемоизация</p>
        <MyMemo />
        <p>побочные эффекты</p>
        <MyEffect />
        <p>Пользовательские хуки</p>
        <Modal />
        {/*<Fetch />*/}
        <MyResize />
        <p>TypeScript Component</p>
        <MyMultyHooksComponent title='Hooks Component' />

    </>
  )
}

export default App
