import { Developer } from "../models/Developer"
  
const developers = [
    { id: 1, name: 'John Doe', grade: 'senior', specialty: 'front' },
    { id: 2, name: 'Jane Smith', grade: 'junior', specialty: 'back' },
    { id: 3, name: 'Alice Johnson', grade: 'middle', specialty: 'back' },
  ];
  
  const getAllDevelopers = () => {
    return developers;
  };
  
  const updateDeveloper = (updatedDeveloper: Developer) => {
    const index = developers.findIndex(dev => dev.id === updatedDeveloper.id);
    if (index !== -1) {
      developers[index] = updatedDeveloper;
    }
  };
  
  const createDeveloper = (newDeveloper: Developer) => {
    const maxId = Math.max(...developers.map(dev => dev.id));
    const newDevWithId = { ...newDeveloper, id: maxId + 1 };
    developers.push(newDevWithId);
    return developers;
  };
  
  export  { getAllDevelopers, updateDeveloper, createDeveloper };
  