import React from 'react';
import { Developer } from "../../models/Developer"
import './style.sass';

const DevelopersList: React.FC<{ developers: Developer[], onDelete: (updatedDevelopers: Developer[]) => void }> = ({ developers, onDelete }) => {
  const handleDelete = (id: number) => {
    const updatedDevelopers = developers.filter(developer => developer.id !== id); 
      onDelete(updatedDevelopers);
    }

    return (
      <div className="list">
        <h2 className="listTitle">Developers List</h2>
        <ul>
          {developers.map(developer => (
            <li key={developer.id}>{developer.name} - {developer.specialty} - {developer.grade}  <button onClick={() => handleDelete(developer.id)}>Delete</button>
            </li>
          ))}
        </ul>
      </div>
    );
  };
  
  export default DevelopersList;