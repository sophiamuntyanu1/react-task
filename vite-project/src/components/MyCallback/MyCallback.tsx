import React, { useState, useCallback } from 'react';

const MyCallback: React.FC = () => {
  const [count, setCount] = useState<number>(0);

  const incrementCount = useCallback(() => {
    setCount(prevCount => prevCount + 1);
  }, []);

  return (
    <div>
      <h1>Count: {count}</h1>
      <button onClick={incrementCount}>Increment Count</button>
    </div>
  );
};

export default MyCallback;
