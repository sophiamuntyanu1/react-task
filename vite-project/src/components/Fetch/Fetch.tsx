import React from 'react';
import useFetchData from '../../hooks/useFetchData';

const Fetch = () => {
  const { data, loading, error } = useFetchData('https://reqres.in/api/users?page=2');

  if (loading) {
      return <div>Loading...</div>;
  }

  if (error) {
      return <div>Error: {error}</div>;
  }

  return (
      <div>
          <h1>Data:</h1>
          <pre>{JSON.stringify(data, null, 2)}</pre>
      </div>
  );
};

export default Fetch;

