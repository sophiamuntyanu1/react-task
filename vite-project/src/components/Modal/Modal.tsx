import React, { useRef, useState } from 'react';
import UseClickOutside from '../../hooks/UseClickOutside';
import './style.sass';


const ModalComponent = () => {
    const modalRef = useRef<HTMLDivElement>(null);
    const [isOpen, setIsOpen] = useState(false);

    UseClickOutside(modalRef, () => {
        setIsOpen(false);
    });

    return (
        <div>
            <button onClick={() => setIsOpen(!isOpen)}>Toggle Modal</button>
            {isOpen && (
                <div className='modal' ref={modalRef}>
                    This is a modal content.
                </div>
            )}
        </div>
    );
};

export default ModalComponent;
