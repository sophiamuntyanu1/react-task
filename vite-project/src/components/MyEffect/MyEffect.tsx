import React, { useEffect, useState } from 'react';

const MyEffect: React.FC = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    // Этот эффект будет выполнен после каждого рендеринга компонента
    console.log('Компонент был отрендерен');

    // Очищаем эффект перед выполнением следующего, если это необходимо 
    // функция, возвращаемая из useEffect, которая выполняется перед следующим вызовом эффекта или перед размонтированием компонента
    return () => {
      console.log('Очистка эффекта');
    };
  }, []);
// 5 Особенности вызова async функций в useEffect
//   useEffect(() => {
//     const fetchData = async () => {
//         try {
//             const response = await fetch('https://api.example.com/data');
//             const result = await response.json();
//             setData(result);
//         } catch (error) { // необходимо учитывать обработку ошибок чтобы предотвратить возможные проблемы
//             console.error('Error fetching data:', error);
//         }
//     };

//     fetchData();
// }, []);
//так нельзя
// useEffect( async () => {
//     const {data} = await axios.get('api/posts')
//     setPosts(data) 
//    }, [])

// Особенности использования объектов и функций в качестве зависимостей в useEffect

// function App() {
//     const [count, setCount] = useState(0)
    
//         const getResult = useCallback(() => {
//             return 2 * 2
//         }, [])
    //getResult — это функция, контрольное значение в памяти воссоздается каждый раз при визуализации компонента,
    // поэтому результат поверхностного сравнения возвращает false
//         useEffect(() => {
//             setCount((count) => count + 1)
//         }, [getResult])
//         // we have specified a function in the dependency array
    
//         return (
//         <div>
//                 <p>value of count: {count}</p>
//         </div>
//         )
//     }


  const increment = () => {
    setCount(count + 1);
  };

  return (
    <div>
      <h1>Простой компонент с хуком useEffect</h1>
      <p>Count: {count}</p>
      <button onClick={increment}>Увеличить</button>
    </div>
  );
};

export default MyEffect;