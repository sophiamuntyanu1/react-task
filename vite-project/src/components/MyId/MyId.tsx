import React, { useId } from 'react';

const MyId: React.FC = () => {
  const passwordHintId = useId();
  return (
    <div>
      <input type="password" aria-describedby={passwordHintId} />
      <p id={passwordHintId}>здесь сгенерированный ключ</p>
    </div>
  );
}

export default MyId;
