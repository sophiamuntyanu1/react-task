import React, { useState, useContext, ChangeEvent, MouseEvent, useRef, FC } from 'react';

// Предполагаем, что контекст имеет тип и значение по умолчанию
interface AppContextType {
  message: string;
}

// Создаём контекст с пустым начальным значением
const AppContext = React.createContext<AppContextType | null>(null);

// Типы для props и children компонента
interface MyComponentProps {
  title: string;
  children?: React.ReactNode;
}

// Создадим интерфейс для типизированного стейта
interface MyComponentState {
  count: number;
}

const MyMultyHooksComponent = ({ title, children }:MyComponentProps) => {
    // Использование useState
    const [state, setState] = useState<MyComponentState>({ count: 0 });
    // const [count, setCount] = useState<number>(0);
  
    // Использование useContext
    const context = useContext<AppContextType>(AppContext);
  
    // Создаем ref для кнопки
    const buttonRef = useRef<HTMLButtonElement | null>(null);
  
    // Обработчик клика,  MouseEvent - это общий тип события мыши в JavaScript, а HTMLButtonElement указывает на тип элемента DOM, на котором произошло событие
    const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
      setState({ count: state.count + 1 });
    };
  
    // Обработчик изменения
    const handleChange:(event: ChangeEvent<HTMLInputElement>) => void = (event) => {
      console.log(event.target.value);
    };
  
    return (
      <div>
        <h1>{title}</h1>
        <p>{context?.message}</p>
        <button onClick={handleClick} ref={buttonRef}>Click me</button>
        <input type="text" onChange={handleChange} />
        {children}
      </div>
    );
  };
  
  export default MyMultyHooksComponent;
  