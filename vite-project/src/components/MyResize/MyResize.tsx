import React from 'react';
import useResize from '../../hooks/useResize';

const MyResize = () => {
  const { width } = useResize();

  return (
    <div>
      <p>Текущая ширина окна: {width}px</p>
    </div>
  );
};

export default MyResize;
