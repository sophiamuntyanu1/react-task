import React, { useState } from 'react';

import { filtersGrade } from '../../models/Filters';
import { filtersSpecialty } from '../../models/FiltersSpecialty';
import './style.sass';
type FilterProps = {
    filter: (grade?: string | undefined, specialty?: string | undefined, name?: string | undefined) => void
};

function Filters({ filter }: FilterProps) {
   
    const [gradeValue, setGradeValue] = useState("");
    const [specialtyValue, setSpecialtyValue] = useState("");
    const [nameValue, setNameValue] = useState("");

    // 1 Пример использования useState в компоненте
    // const Counter = () => {
    //     const [count, setCount] = useState(0);
    //
    //     const increment = () => {
    //         setCount(count + 1);
    //     };
    //
    //     return (
    //         <div>
    //             <h1>{count}</h1>
    //             <button onClick={increment}>Increment</button>
    //         </div>
    //     );
    // };

    //2 Какими способами можно передать начальное значение
    // 1) const [count, setCount] = useState(0);
    // 2) Использование функции для передачи значения в useState
    // const initialCount = 0;
    // const [count, setCount] = useState(() => initialCount);
    // 3) Передача значения переменной или константы в useState
    // const initialCount = 0;
    // const [count, setCount] = useState(initialCount);

    // 3 setState- как происходит слияние нового состояния со старым, как модифицировать предыдущее состояние?
    // 1) Передача нового значения
    // const [count, setCount] = useState(0);
    // setCount(2);
    // 2) Использование функции обновления
    // const [count, setCount] = useState(0);
    // setCount(prevCount => prevCount + 1);


    // Есть два способа модификации предыдущего состояния с помощью setState:

    // 1. Передача колбэка в setState:
    // this.setState((prevState) => {
    // return { count: prevState.count + 1 };
    // });
    // В этом случае колбэк получает предыдущее состояние в качестве аргумента и возвращает новый объект состояния, который объединяется с предыдущим.

    // 2. Передача объекта в setState:
    // this.setState({ count: this.state.count + 1 });

   
    const handleGradeChange = (grade: any) => {
        setGradeValue(grade)
        filter(grade, specialtyValue, nameValue)
    };

    const handleSpecialtyChange = (specialty: any) => {
        setSpecialtyValue(specialty)
        filter(gradeValue, specialty, nameValue)
    };
    
    const handleNameChange = (e: any) => {
        const searchValue = e.target.value;
        setNameValue(searchValue)
        filter(gradeValue, specialtyValue, searchValue)
    };

    return (
        <div className='filters'>
            <div className='filtersGrade'>
            <div className='filtersGradeTitle'>Choose a grade</div>
            {filtersGrade.map(({ id, grade }) => {
                return (
                    <>
                     <button
                        key={id}
                        onClick={() => handleGradeChange(grade)}
                    >
                        <div>
                            <div>{grade}</div>
                        </div>
                    </button>
                   
                    </>
                );
            })}
            </div>

            <div className='filtersSpecialty'>
                <div className='filtersSpecialtyTitle'>Choose a specialty</div>
                {filtersSpecialty.map(({ id, specialty }) => {
                    return (
                        <>
                        <button
                            key={id}
                            onClick={() => handleSpecialtyChange(specialty)}
                        >
                            <div>
                                <div>{specialty}</div>
                            </div>
                        </button>
                    
                        </>
                    );
                })}
            </div>

            <input type="text" placeholder="Поиск по имени" onChange={handleNameChange} />

        </div>
    );
}

export default Filters;
