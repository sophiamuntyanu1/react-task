import React, { useState, useMemo } from 'react';

//Можно ли мемоизировать JSX элементы?
// const MyComponent = ({ name }) => {
//     const memoizedElement = useMemo(() => (
//         <div>
//             <h1>Hello, {name}!</h1>
//             <p>This is a memoized JSX element.</p>
//         </div>
//     ), [name]);
//
//     return (
//         <div>
//             {memoizedElement}
//         </div>
//     );
// };

const MyMemo: React.FC = () => {
  const [value, setValue] = useState<number>(0);
  
  const squaredValue = useMemo(() => {
    console.log('Calculating squared value');
    return value * value;
  }, [value]);

  return (
    <div>
      <h1>Value: {value}</h1>
      <h2>Squared Value: {squaredValue}</h2>
      <button onClick={() => setValue(value + 1)}>Increment Value</button>
    </div>
  );
};

export default MyMemo;
