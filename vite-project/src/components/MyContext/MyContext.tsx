import React, { useContext, useState } from 'react';

// 2 Как изменить значение контекста?
// const MyContext = createContext('Initial Value');
// const App = () => {
//     const value = useContext(MyContext);
//     return (
//         <div>
//             <h1>Value from context: {value}</h1>
//         </div>
//     );
// };
// const WrapperComponent = () => {
//     return (
//         <MyContext.Provider value=value>
//             <App />
//         </MyContext.Provider>
//     );
// };

const context = React.createContext<string>('initial value');

const MyContext: React.FC = () => {
  const [value, setValue] = useState<string>(useContext(context));

  const handleUpdateContext = (newValue: string) => {
    setValue(newValue);
  };

  return (
    <div>
      <p>Value from Context: {value}</p>
      <button onClick={() => handleUpdateContext('New Value')}>
        Update Context
      </button>
    </div>
  );
}

export default MyContext;
