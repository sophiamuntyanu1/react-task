import React, { useRef } from 'react';

interface Item {
  id: number;
  text: string;
}

const MyRef: React.FC = () => {
  const items: Item[] = [
    { id: 1, text: 'Item 1' },
    { id: 2, text: 'Item 2' },
    { id: 3, text: 'Item 3' },
  ];

  const itemRefs = useRef<(HTMLLIElement | null)[]>([]);

  const handleClick = (index: number) => {
    const itemNode = itemRefs.current[index];
    if (itemNode) {
      console.log('Clicked item at index', {index}, itemNode.textContent);
    }
  };

  // 3 Как изменять значение? Стоит ли изменять во время рендера?
  // const myRef = useRef(initialValue);
  // myRef.current = newValue;

  // 4 Как пробросить ref useRef по элементам если рендерится список элементов? Как быть, если нужен доступ к определенной ноде из списка?
  // 1) использоваться колбэк-функцией ref
  // const myRef = useRef();
  // const nodes = items.map((item, index) => (
  //     <div key={index} ref={index === 2 ? myRef : null}>
  //       {item}
  //     </div>
  // ));
  // 2) доступ к определенной ноде из списка
  //   const refs = useRef([]);
  //   const nodes = items.map((item, index) => (
  //       <div key={index} ref={el => (refs.current[index] = el)}>
  //         {item}
  //       </div>
  //   ));
  // // Доступ к ноде по индексу
  //   const node = refs.current[2];
  // // Доступ к ноде по ключу
  //   const nodeByKey = refs.current[key];


  //5 Как пробросить ref в свой компонент?
  // import React, { forwardRef, useRef } from 'react';
  // const MyComponent = forwardRef((props, ref) => {
  //   const inputRef = useRef();
  //   return (
  //       <input ref={inputRef} />
  //   );
  // });
  // export default MyComponent;
  // а можно так
  // const ParentComponent = () => {
  //   const myComponentRef = useRef();
  //   return (
  //       <div>
  //         <MyComponent ref={myComponentRef} />
  //         <button onClick={() => myComponentRef.current.focus()}>Фокус</button>
  //       </div>
  //   );
  // };

  //6 Пример передачи нескольких ref в компонент:
  // const MyComponent = ({ refs }) => {
  //   return (
  //       <div>
  //         <input ref={refs.inputRef} />
  //         <button ref={refs.buttonRef}>Click me</button>
  //       </div>
  //   );
  // };
  // const App = () => {
  //   const inputRef = useRef(null);
  //   const buttonRef = useRef(null);
  //
  //   const refs = {
  //     inputRef,
  //     buttonRef,
  //   };
  //   return <MyComponent refs={refs} />;
  // };


  return (
    <ul>
      {items.map((item, index) => (
        <li
          key={item.id}
          ref={(node) => (itemRefs.current[index] = node)}
          onClick={() => handleClick(index)}
        >
          {item.text}
        </li>
      ))}
    </ul>
  );
};

export default MyRef;


