import React, { useState } from 'react';

import { Developer } from "../../models/Developer"
import { getAllDevelopers, createDeveloper }  from '../../services/developerService';
import './style.sass';

const AddDeveloper: React.FC<{ onAdd: (developers: Developer[]) => void }> = ({ onAdd }) => {
    const [name, setName] = useState('');
    const [specialty, setSpecialty] = useState('');
    const [grade, setGrade] = useState('');
  
    const handleAdd = () => {

      const  developers =  getAllDevelopers();
      const newDeveloper: Developer = {
        id: developers.length + 1,
        name: name,
        grade: grade,
        specialty: specialty
      };
      
      
      const newDevelopers =  createDeveloper(newDeveloper);
      console.log(newDevelopers)
      setName('');
      setSpecialty('');
      setGrade('')
      onAdd(newDevelopers)
    };
  
    return (
      <div>
        <h2>Add Developer</h2>
        <div className='add-developer'>
          <input type="text" value={name} onChange={e => setName(e.target.value)} placeholder="Name" />
          <input type="text" value={specialty} onChange={e => setSpecialty(e.target.value)} placeholder="Specialty" />
          <input type="text" value={grade} onChange={e => setGrade(e.target.value)} placeholder="Grade" />
          <button onClick={handleAdd}>Add Developer</button>
        </div>
      </div>
    );
  };

  export default AddDeveloper;